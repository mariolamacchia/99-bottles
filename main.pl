use strict;
use warnings;


sub printNumOfBeers {
  my $beers = $_[0];
  if ($_[0] == 0) {
    $beers = "No one";
  }
  
  my $s = "s";
  if ($_[0] <= 1) {
    $s = "";
  }

  my $onTheWall = "!";
  if ($_[1]) {
    $onTheWall = " on the wall!";
  }

  print "$beers bottle$s of beer$onTheWall\n";
}


sub printSong {
  for (my $i = $_[0]; $i > 0; $i--) {
    printNumOfBeers($i, 1);
    printNumOfBeers($i, 0);
    print "Take one down, pass it around\n";
    printNumOfBeers($i-1, 1);
    print "\n";
  }
  printNumOfBeers(0, 1);
  printNumOfBeers(0, 0);
  print "Go to the shop and buy some more\n";
  printNumOfBeers($_[0], 1);
  print "\n";
}


printSong(99)
