def printNumOfBeers(beers, onTheWall):
    s = ''
    if beers > 0:
        s = str(beers)
    else:
        s = 'No one'

    s = s + ' bottle'
    if beers > 1:
        s = s + 's'

    s = s + ' of beer'

    if onTheWall == 1:
        s = s + ' on the wall!'
    else:
        s = s + '!'

    print(s)


def printSong(beers):
    i = beers
    while i > 0:
        printNumOfBeers(i, 1)
        printNumOfBeers(i, 0)
        print("Take one down, pass it around")
        printNumOfBeers(i-1, 1)
        print('')
        i = i - 1
    printNumOfBeers(0, 1)
    printNumOfBeers(0, 0)
    print("Go to the shop and buy some more")
    printNumOfBeers(beers, 1)
    print('')


printSong(99)
