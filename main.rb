def printNumOfBeers(beers, onTheWall)
    if beers > 0 then print beers else print "No one" end
    print " bottle"
    if beers > 1 then print "s" end
    print " of beer"
    if onTheWall == true then print " on the wall!" else print "!" end 
    puts
end


def printSong(beers)
    i = beers
    while i > 0
        printNumOfBeers(i, true)
        printNumOfBeers(i, false)
        puts "Take one down, pass it around"
        printNumOfBeers(i-1, true)
        puts ''
        i = i - 1
    end
    printNumOfBeers(0, true)
    printNumOfBeers(0, false)
    puts "Go to the shop and buy some more"
    printNumOfBeers(beers, true)
    puts
end


printSong(99)
