(defun print-num-of-beers (beers on-the-wall)
  (format t "~A bottle~A of beer~A~%"
    (if (> beers 0) (write-to-string beers) "No one")
    (if (> beers 1) "s" "")
    (if (eq on-the-wall t) " on the wall!" "!")))

(defun print-song (beers)
  (loop for i from beers downto 0 do
    (print-num-of-beers i t)
    (print-num-of-beers i nil)
    (format t (if (> i 0)
        "Take one down, pass it around~%" 
      "Go to the shop and buy some more~%"))
    (let ((i (if (> i 0) (- i 1) beers)))
      (print-num-of-beers i t)
      (format t "~%"))))

(print-song 99)
