def printNumOfBeers(beers: Int, onTheWall: Boolean) = {
    var bottles: String = "No one"
    if (beers > 0) bottles = "%d".format(beers)

    var plural: String = ""
    if (beers > 1) plural = "s"

    var eos: String = "!"
    if (onTheWall) eos = " on the wall!"
      
    println("%s bottle%s of beer%s".format(bottles, plural, eos))
}


def printSong(beers: Int) = {
    for (i <- beers to 1 by -1) {
        printNumOfBeers(i, true)
        printNumOfBeers(i, false)
        println("Take one down, pass it around")
        printNumOfBeers(i-1, true)
        println()
    }
    printNumOfBeers(0, true)
    printNumOfBeers(0, false)
    println("Go to the shop and buy some more")
    printNumOfBeers(beers, true)
    println()
}


printSong(99)
