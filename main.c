#include <stdio.h>


/*
 * printNumOfBeers: print the row: "%d bottle[s] of beer[ on the wall]!"
 *
 *  input:
 *      beers {int}: number of bottles of beer
 *      onTheWall {int}: not 0 if function must print also "on the wall"
 *
 *  output: {void}
 */
void printNumOfBeers(int beers, int onTheWall)
{
    if (beers > 1) printf("%d bottles of beer", beers);
    else if (beers == 1) printf("1 bottle of beer");
    else printf("No one bottle of beer");

    if (onTheWall) printf(" on the wall!\n");
    else printf("!\n");
}


/*
 * printSong: print song "99 bottles of beer on the wall"
 *
 *  input:
 *      beers {int}: starting (and ending number of beers)
 *
 *  output: {void}
 */
void printSong(int beers)
{
    int i;
    for (i = beers; i > 0; i--)
    {
        printNumOfBeers(i, 1);
        printNumOfBeers(i, 0);
        printf("Take one down, pass in around,\n");
        printNumOfBeers(i-1, 1);
        // Newline at the end of verse
        printf("\n");
    }
    printNumOfBeers(0, 1);
    printNumOfBeers(0, 0);
    printf("Go to the shop and buy some more,\n");
    printNumOfBeers(beers, 1);
}


int main()
{
    printSong(99);
    return 0;
}
