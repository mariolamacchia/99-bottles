function printNumOfBeers(beers, onTheWall)
  if beers > 1 then
    s = "s"
  else
    s = ""
  end

  if beers > 0 then
    beers = tostring(beers)
  else
    beers = "No one"
  end

  if onTheWall == 1 then
    onTheWall = " on the wall!"
  else
    onTheWall = "!"
  end

  print(beers .. " bottle" .. s .. " of beer" .. onTheWall)
end


function printSong(beers)
  i = beers
  while i > 0 do
    printNumOfBeers(i, 1)
    printNumOfBeers(i, 0)
    print("Take one down, pass it around")
    printNumOfBeers(i-1, 1)
    print("")
    i = i - 1
  end
  printNumOfBeers(0, 1)
  printNumOfBeers(0, 0)
  print("Go to the shop and buy some more")
  printNumOfBeers(beers, 1)
  print("")
end


printSong(99)
