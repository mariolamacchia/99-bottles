package main

import "fmt"


func printNumOfBeers(beers int, onTheWall bool) {
    if beers > 1 {
        fmt.Print(beers, " bottles of beer")
    } else if beers == 1 {
        fmt.Print("1 bottle of beer")
    } else {
        fmt.Print("No one bottle of beer")
    }

    if onTheWall {
        fmt.Println(" on the wall!")
    } else {
        fmt.Println("!")
    }
}


func printSong(beers int) {
    i := beers
    for i > 0 {
        printNumOfBeers(i, true)
        printNumOfBeers(i, false)
        fmt.Println("Take one down, pass it around")
        printNumOfBeers(i-1, true)
        fmt.Println()
        i = i - 1
    }
    printNumOfBeers(0, true)
    printNumOfBeers(0, false)
    fmt.Println("Go to the shop and buy some more")
    printNumOfBeers(beers, true)
    fmt.Println()
}


func main() {
    printSong(99);
}
