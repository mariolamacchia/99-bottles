<?php

function printNumOfBeers($beers, $onTheWall) {
  if ($beers > 1) $s = 's';
  else $s = '';
  if ($beers == 0) $beers = 'No one';
  if ($onTheWall) $onTheWall = ' on the wall!';
  else $onTheWall = '!';
  echo("$beers bottle$s of beer$onTheWall\n");
}


function printSong($beers) {
    for ($i = $beers; $i > 0; $i--) {
        printNumOfBeers($i, true);
        printNumOfBeers($i, false);
        echo("Take one down, pass it around\n");
        printNumOfBeers($i-1, true);
        echo("\n");
    }
    printNumOfBeers(0, true);
    printNumOfBeers(0, false);
    echo("Go to the shop and buy some more\n");
    printNumOfBeers($beers, true);
}


printSong(99);
