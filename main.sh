function printNumOfBeers {
  if [ $1 -gt 0 ]; then
    beers=$1
  else
    beers="No one"
  fi

  if [ $1 -gt 1 ]; then
    s=s
  else
    s=
  fi

  if ($2); then
    onTheWall=' on the wall!'
  else
    onTheWall='!'
  fi

  echo $beers bottle$s of beer$onTheWall
}


function printSong {
  i=$1
  while [ $i -gt 0 ]; do
    printNumOfBeers $i true
    printNumOfBeers $i false
    echo Take one down, pass it around
    printNumOfBeers $((i-1)) true
    echo
    i=$((i-1))
  done
  printNumOfBeers 0 true
  printNumOfBeers 0 false
  echo Go to the shop and buy some more
  printNumOfBeers $1 true
}


printSong 99
