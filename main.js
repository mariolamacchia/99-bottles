'use strict'


function printNumOfBeers(beers, onTheWall) {
    console.log(
        (beers || "No one") +
        " bottle" + (beers > 1 ? 's' : '') +
        " of beer" +
        (onTheWall ? " on the wall!" : "!")
    );
}


function printSong(beers) {
    for (var i = beers; i > 0; i--) {
        printNumOfBeers(i, true);
        printNumOfBeers(i, false);
        console.log("Take one down, pass it around");
        printNumOfBeers(i-1, true);
        console.log();
    }
    printNumOfBeers(0, true);
    printNumOfBeers(0, false);
    console.log("Go to the shop and buy some more");
    printNumOfBeers(beers, true);
    console.log();
}


printSong(99);
